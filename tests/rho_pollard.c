#include <stdio.h>
#include "bignum.h"

unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = 1;
  if(a < b)
    return gcd(b,a);

  while(r != 0){
    old_r = r;
    r = a % b;
    a = b;
    b = r;
  }

  return old_r;
}

unsigned long int f(unsigned long int x){
  return(x*x+1);
}


unsigned long int rho_pollard(unsigned long int n){
    unsigned long int x = 2l;
    unsigned long int y = 2l;
    unsigned long int d = 1l;
    while(d == 1){
        x = f(x) % n;
        y = f(f(y)) % n;
        //printf("%d, %d",x,y);
        d = gcd(x-y, n);
      }
    return d;
  }

/* -------------------------- Version Big Num ------------------------------*/

unsigned long int gcd_big(unsigned long int a, unsigned long int b){
    unsigned long int old_r, r = 1;
    if(a < b)
      return gcd(b,a);

    while(r != 0){
      old_r = r;
      r = a % b;
      a = b;
      b = r;
    }

    return old_r;
}

unsigned long int f_big(unsigned long int x){
    return(x*x+1);
}

bignum_t rho_pollard_big(bignum_t n){
      bignum_t x = 2;
      bignum_t y = 2;
      bignum_t d = 1;
      while(d == 1){
          x = f(x) % n;
          y = f(f(y)) % n;
          //printf("%d, %d",x,y);
          d = gcd(x-y, n);
        }
      return d;
    }

int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17 * 113;
  unsigned long int n2 = 239 * 431;
  unsigned long int n3 = 3469 * 4363;
  unsigned long int n4 = 15241 * 18119;
  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;

  bignum_t n7, n8;

  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));

  printf("%ld \n", rho_pollard(n1));
  printf("%ld \n", rho_pollard(n2));
  printf("%ld \n", rho_pollard(n3));
  printf("%ld \n", rho_pollard(n4));
  printf("%ld \n", rho_pollard(n5));
  printf("%ld \n", rho_pollard(n6));
  printf("%ld \n", rho_pollard(n7));
  printf("%ld \n", rho_pollard(n8));

  printf("PGCD(42,24) = %lu\n", gcd(42,24));
  printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));

  return 0;
}
